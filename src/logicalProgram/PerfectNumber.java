package logicalProgram;

import java.util.Scanner;

public class PerfectNumber {

	// perfect number
	// sum of all divisible to the number = number
	// a increment if condition either true or false
	// number divide if modulo ==0
	// first write a function
	// if user respose yes then method call again

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {
			int response = 0;
			do {
				perfect();
				System.out.println("Do you want to continue : if yes press 5 " + "if not press any key ");
				response = sc.nextInt();
			} while (response == 5);
		}
		{
		}
	}

	public static boolean perfect() {

		System.out.println("Enter your number ");
		try (Scanner sc = new Scanner(System.in)) {
			int number = sc.nextInt();
			int a = 1;
			int sum = 0;
			int total = 0;
			while (number > a) {
				a++;
				if (number % a == 0) {
					total = number / a;
					sum = sum + total;
				}
			}
			System.out.println("Sum of divisible is :" + sum);
			if (sum == number) {
				System.out.println("Number is perfect !!");
				return true;
			} else {
				System.out.println("Number is not perfect !!");
				return false;
			}
		}
	}
}
