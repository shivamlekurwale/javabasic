package logicalProgram;

public class FibbonacciSeries {

	public static void main(String[] args) {
		// Using for loop number should be increment
		// In privios number we add next number
		int number = 9;
		int n1 = 0, n2 = 1, n3;
		for (int i = 1; i < number; i++) {
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
			System.out.println(n3);
		}
	}
}
