package logicalProgram;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GradeCalculater {
	private List<Integer> markList = new ArrayList<Integer>();
	private int number;
	private boolean offer = true;

	public static void main(String[] args) {
		GradeCalculater gradeCalculater = new GradeCalculater();
		gradeCalculater.getMarks();
		float pr = gradeCalculater.calculatePercentage();
		gradeCalculater.calculateGrade(pr);
	}

	public List<Integer> getMarkList() {
		return markList;
	}

	public void getMarks() {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Enter your Physics marks");
			markList.add(0, sc.nextInt());
			System.out.println("Enter your Chemistry marks");
			markList.add(1, sc.nextInt());
			System.out.println("Enter your  maths marks");
			markList.add(2, sc.nextInt());
			System.out.println("Enter your  Biology marks");
			markList.add(3, sc.nextInt());
			System.out.println("Enter your  marathi marks");
			markList.add(4, sc.nextInt());
		}
		if (offer) {
			getNationalCertificateInd();
		}

	}

	public void getNationalCertificateInd() {
		try (Scanner sc = new Scanner(System.in)) {
			System.out.println("Do you have national level certificate in sport " + "\n"
					+ " if yes then press 1 ,if not then press any number");
			number = sc.nextInt();
		}
	}

	public float calculatePercentage() {
		float percentage = (this.markList.get(0) + this.markList.get(1) + this.markList.get(2) + this.markList.get(3)
				+ this.markList.get(4)) / 5f;

		return percentage + (number == 1 ? 3 : 0);
	}

	public void calculateGrade(float percentage) {
		System.out.println(percentage);
		if (percentage > 91) {
			System.out.println("Congratulation !! " + "You are in Distinction ");
		} else if (percentage < 90.99f && percentage > 75f) {
			System.out.println("Congratulation !! " + "You are in First grade ");
		} else if (percentage < 74.99f && percentage > 61f) {
			System.out.println("Congratulation !! " + "You are in Second grade ");
		} else if (percentage < 60.99f && percentage > 45f) {
			System.out.println("Congratulation !! " + "You are in Third grade ");
		} else if (percentage < 46.0f && percentage > 35f) {
			System.out.println("Congratulation !! " + "You are in Fourth grade ");
		} else {
			System.out.println("You are fail !!!!");
		}
	}
}
