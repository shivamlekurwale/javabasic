package logicalProgram;

import java.util.HashSet;
import java.util.Set;

public class DuplicateNumber {

	public static void main(String[] args) {
		// array with number of sets
		// compare each number with another number in the array
		// take 2 for loops
		// if number matches print the number
		// int a[] = { 2, 3, 4, 5, 5, 6, 7, 8, 34,2 };
		// int temp = 0;
		// for (int i = 0; i < a.length; i++) {
		// for (int j = 0; j < i; j++) {
		// if (a[i] == a[j]) {
		// System.out.println(a[i]);
		// break;
		// }
		// }
		// }
		DuplicateNumber.arrayList();
	}

	public static void arrayList() {
		Set<Integer> hashSet = new HashSet<Integer>();
		int a[] = { 2, 3, 4, 5, 5, 6, 7, 8, 34, 2 };
		for (int i = 0; i < a.length; i++) {
			if (!hashSet.add(a[i])) {
				System.out.println(a[i]);
			}
		}
	}
}
