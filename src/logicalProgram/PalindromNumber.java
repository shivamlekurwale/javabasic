package logicalProgram;

public class PalindromNumber {

	public static void main(String[] args) {
		//
		int n = 121;
		int sum = 1;
		while (n > 0) {
			sum = n%10;
			sum = n/10;
		}
		System.out.println(sum);
	}
}
