package logicalProgram;

public class PrimeNumber {

	public static void main(String[] args) {
		// Write a program (WAP) to
		// check if given number is prime or not
		// The number is only devided by itself
		// we use one if loop
		// we use one for loop

		// def: the number which is divisible by 1 and itself only such numbers are
		// known as prime number
		// start dividing the number with 2 to n-1
		// if given number is divided by any number then it is not prime number
		// if it is not divided by any number then it is prime number

		int number = 58;
		int a = 1;
		int i = 2;
		int b = 0;
		while(number>a)
			a++;
		for (i = 2; i <number ; i++) {
			if( number % i==0) {
			b = number / i;
			}
		}
		 if(b>1) {
			 System.out.println("Number is not prime");
		 }else {
			 System.out.println("Number is prime");
		 }
	}
}
