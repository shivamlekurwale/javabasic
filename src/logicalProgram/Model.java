package logicalProgram;

public class Model  {
	public int sum = 1;
	public float percentage = 1;
	public int number;
	public float percentagePlus = 0;
	public int physics,chemistry, maths, biology, marathi;
	public Model() {
		super();
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
	}
	public float getPercentage() {
		return percentage;
	}
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public float getPercentagePlus() {
		return percentagePlus;
	}
	public void setPercentagePlus(float percentagePlus) {
		this.percentagePlus = percentagePlus;
	}
	public int getPhysics() {
		return physics;
	}
	public void setPhysics(int physics) {
		this.physics = physics;
 	}
	public int getChemistry() {
		return chemistry;
	}
	public void setChemistry(int chemistry) {
		this.chemistry = chemistry;
	}
	public int getMaths() {
		return maths;
	}
	public void setMaths(int maths) {
		this.maths = maths;
	}
	public int getBiology() {
		return biology;
	}
	public void setBiology(int biology) {
		this.biology = biology;
	}
	public int getMarathi() {
		return marathi;
	}
	public void setMarathi(int marathi) {
		this.marathi = marathi;
	}
	 
 }
