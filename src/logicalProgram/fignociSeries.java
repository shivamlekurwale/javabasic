package logicalProgram;

import java.util.Scanner;

public class fignociSeries {

	public static void main(String[] args) {
		try (Scanner sc = new Scanner(System.in)) {
			int a = 9;
			int n1 = 0, n2 = 1, n3;
			System.out.println(n1 + " " + n2);
			for (int i = 2; i < a; ++i) {
				n3 = n1 + n2;
				System.out.print(" " + n3);
				n1 = n2;
				n2 = n3;
			}
		}
	}

}
