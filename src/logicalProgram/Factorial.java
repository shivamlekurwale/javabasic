package logicalProgram;

public class Factorial {

	public static void main(String[] args) {
		// Using for loop multiplying the number
		int number = 8;
		int n = 1;
 		for (int i = 1; i <= number; i++) {
			 n = n * i;
		}
		System.out.println(n);

	}

}
